package minecraft;

public class Piedra extends Material{
    Herramienta herramienta;
    
    public Piedra(Herramienta herramienta){
        this.herramienta = herramienta;
        duracion=durabilidad();
    }
    
    public String getDescripcion(){
        return herramienta.getDescripcion() + ", Piedra";
    }

    public int durabilidad() {
        return 10 + herramienta.durabilidad();
    }
    
}
