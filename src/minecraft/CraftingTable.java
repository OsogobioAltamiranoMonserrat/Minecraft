package minecraft;

public class CraftingTable {
    FabricaHerramientas fabricaHerramientas;
    
    public CraftingTable(FabricaHerramientas fabricaHerramientas){
        this.fabricaHerramientas = fabricaHerramientas;
    }
    
    public Herramienta crearHerramienta(String tipo){
        Herramienta herramienta;
        herramienta= fabricaHerramientas.generarHerramienta(tipo);
        return herramienta;
    }
}
