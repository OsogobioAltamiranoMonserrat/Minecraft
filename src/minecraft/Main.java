package minecraft;

public class Main {
    
    public static void main (String [] arg){
        FabricaHerramientas fabricaHerramientas = new FabricaHerramientas();
        CraftingTable craftingTable = new CraftingTable(fabricaHerramientas);
        CajaHerramientas cajaHerramientas = new CajaHerramientas(5);
        
        //-----------------Creación de Elementos
        Herramienta martillo = craftingTable.crearHerramienta("Martillo");   
        Herramienta clavo = craftingTable.crearHerramienta("Clavo");
        Herramienta desarmador = craftingTable.crearHerramienta("Desarmador");
        Herramienta tachuela = craftingTable.crearHerramienta("Tachuela");
        Herramienta pinza = craftingTable.crearHerramienta("Pinza");
        Herramienta tornillo = craftingTable.crearHerramienta("Tornillo");
        
        System.out.println(clavo.getDescripcion());
        System.out.println(desarmador.getDescripcion());
        System.out.println(tachuela.getDescripcion());
        System.out.println(pinza.getDescripcion());
        System.out.println(tornillo.getDescripcion());
       
        //--------------------AGREGANDO MATERIALES
        System.out.println("AGREGANDO MATERIALES");
       
        clavo = new Piedra(clavo);
        System.out.println(clavo.getDescripcion());
        martillo = new Oro(martillo);
        martillo = new Oro(martillo);
        martillo = new Piedra(martillo);
        martillo = new Diamante(martillo);
        martillo = new Diamante(martillo);
        martillo = new Madera(martillo);         
        System.out.println(martillo.getDescripcion());
        
        
        System.out.println("AGREGAR A LA CAJA");
        
        System.out.println(cajaHerramientas.agregar(martillo));
        System.out.println(cajaHerramientas.agregar(clavo));
        System.out.println(cajaHerramientas.agregar(pinza));
        System.out.println(cajaHerramientas.agregar(desarmador));
        System.out.println(cajaHerramientas.agregar(tornillo));
        System.out.println(cajaHerramientas.agregar(tachuela));
        
        System.out.println(martillo.getDuracion());
        martillo.uso();
        martillo.uso();
        System.out.println(martillo.getDuracion());
        
        System.out.println(clavo.getDuracion());
        clavo.uso();
        clavo.uso();
        System.out.println(clavo.getDuracion());
    }
    
}
