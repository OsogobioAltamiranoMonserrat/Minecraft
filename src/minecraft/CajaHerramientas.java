package minecraft;

import java.util.ArrayList;

public class CajaHerramientas {
    private ArrayList<Herramienta> herramientas = new ArrayList<Herramienta>();
    private int capacidad;
    
    public CajaHerramientas (int capacidad) {
        this.capacidad = capacidad;
    }
    
    public String agregar (Herramienta herramienta){
        if(herramientas.size() < this.capacidad){
            herramientas.add(herramienta);
            return herramienta.getDescripcion()+" agregado";
        }else{
         return "Caja Llena";
        }
    }
}
