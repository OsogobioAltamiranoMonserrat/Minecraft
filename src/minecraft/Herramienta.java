package minecraft;

public abstract class Herramienta {
    String descripcion;
    int duracion;
    
    public String getDescripcion(){
        return descripcion;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }
   
    public void uso(){
    duracion=duracion-1;
    }
    
    public abstract int durabilidad();
}
