package minecraft;

public class Oro extends Material{
    Herramienta herramienta;
    
    public Oro(Herramienta herramienta){
        this.herramienta = herramienta;
        duracion=durabilidad();
    }
    
    public String getDescripcion(){
        return herramienta.getDescripcion() + ", Oro";
    }

    public int durabilidad() {
        return 32 + herramienta.durabilidad();
    }
    
}
