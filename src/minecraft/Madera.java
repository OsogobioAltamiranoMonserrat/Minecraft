package minecraft;

public class Madera extends Material{
    Herramienta herramienta;
    
    public Madera(Herramienta herramienta){
        this.herramienta = herramienta;
        duracion=durabilidad();
    }
    
    public String getDescripcion(){
        return herramienta.getDescripcion() + ", Madera";
    }

    public int durabilidad() {
        return 5 + herramienta.durabilidad();
    }
    
}
