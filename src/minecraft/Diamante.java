package minecraft;

public class Diamante extends Material{
    Herramienta herramienta;
    
    public Diamante(Herramienta herramienta){
        this.herramienta = herramienta;
        duracion=durabilidad();
    }
    
    public String getDescripcion(){
        return herramienta.getDescripcion() + ", Diamante";
    }

    public int durabilidad() {
        return 50 + herramienta.durabilidad();
    }
    
}
