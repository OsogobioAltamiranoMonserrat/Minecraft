package minecraft;

public class FabricaHerramientas {
    
    public Herramienta generarHerramienta(String tipo){
        Herramienta herramienta = null;
        
        if(tipo.equals("Martillo")){
            herramienta = new Martillo();
        }else if(tipo.equals("Clavo")){
            herramienta = new Clavo();
        }else if(tipo.equals("Desarmador")){
            herramienta = new Desarmador();
        }else if(tipo.equals("Pinza")){
            herramienta = new Pinza();
        }else if(tipo.equals("Tachuela")){
            herramienta = new Tachuela();
        }else if(tipo.equals("Tornillo")){
            herramienta = new Tornillo();
        }
        return herramienta;
    }
}
